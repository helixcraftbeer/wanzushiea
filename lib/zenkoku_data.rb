require 'csv'
local_file = "#{Rails.root}/lib/zenkoku.csv"
CSV.foreach(local_file, headers: false) do |row|
  if Prefecture.find_by(name: row[0])
    prefecture_id = Prefecture.find_by(name: row[0]).id
  else
    prefecture = Prefecture.new
    prefecture.name = row[0]
    prefecture.save
    prefecture_id = prefecture.id
    prefecture.update(uniq_name: "PRE" + prefecture_id.to_s)
  end
  if City.find_by(name: row[2])
    unless City.find_by(prefecture_id: prefecture_id)
      city = City.new
      city.name = row[2]
      city.prefecture_id = prefecture_id
      city.save
      city_id = city.id
      city.update(uniq_name: "CIT" + city_id.to_s)
    end
  else
    city = City.new
    city.name = row[2]
    city.prefecture_id = prefecture_id
    city.save
    city_id = city.id
    city.update(uniq_name: "CIT" + city_id.to_s)
  end
end
