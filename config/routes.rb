Rails.application.routes.draw do
  devise_for :users, :controllers => {
    :sessions      => "users/sessions",
    :registrations => "users/registrations",
    :passwords     => "users/passwords",
  }
  root 'home#index'
  get '/home_serach' => 'home#home_serach'

  resources :abouts, only: [:index]
  resources :searchs, only: [:index]
  resources :car_searchs, only: [:index]
  get '/detail_search' => 'car_searchs#detail_search'
  resources :questions, only: [:index]
  resources :news, only: [:index]
  resources :stores, only: [:index]
  resources :cars, only: [:index]
  post '/reserve' => 'cars#reserve'
  get '/reserved' => 'cars#reserved'

  resources :mypages, only: [:index]
  get '/profile' => 'mypages#profile'
  post '/profile_create' => 'mypages#profile_create'
  patch '/profile_update' => 'mypages#profile_update'
  get '/license' => 'mypages#license'
  post '/license_create' => 'mypages#license_create'
  patch '/license_update' => 'mypages#license_update'
  get '/review' => 'mypages#review'
  post '/review_create' => 'mypages#review_create'
  get '/notification' => 'mypages#notification'
  post '/notification_set' => 'mypages#notification_set'
  get '/payment' => 'mypages#payment'
  get '/privacy' => 'mypages#privacy'
  get '/password' => 'mypages#password'
  post '/password_set' => 'mypages#password_set'
  get '/detail' => 'mypages#detail'
  post '/detail_set' => 'mypages#detail_set'
  get '/reservation' => 'mypages#reservation'

  resources :ownerpages, only: [:index]
  get '/entry' => 'ownerpages#entry'
  post '/owner_create' => 'ownerpages#owner_create'
  get '/performance' => 'ownerpages#performance'
  get '/revenue' => 'ownerpages#revenue'
  get '/consider' => 'ownerpages#consider'
  get '/not_approval' => 'ownerpages#not_approval'
  get '/not_acceptance' => 'ownerpages#not_acceptance'
  get '/car_edit' => 'ownerpages#car_edit'
  patch '/car_update' => 'ownerpages#car_update'
  get '/owner_profile' => 'ownerpages#owner_profile'
  patch '/owner_profile_update' => 'ownerpages#owner_profile_update'
  get '/owner_license' => 'ownerpages#owner_license'
  patch '/owner_license_update' => 'ownerpages#owner_license_update'
  get '/owner_notification' => 'ownerpages#owner_notification'
  get '/owner_account' => 'ownerpages#owner_account'
  post '/owner_account_create' => 'ownerpages#owner_account_create'
  patch '/owner_account_update' => 'ownerpages#owner_account_update'
  get '/owner_privacy' => 'ownerpages#owner_privacy'
  get '/owner_password' => 'ownerpages#owner_password'
  get '/owner_detail' => 'ownerpages#owner_detail'
  post '/store_select' => 'ownerpages#store_select'


  resources :reserve_conditions, only: [:index]
  get '/reserve01' => 'reserves#reserve01'
  get '/reserve02' => 'reserves#reserve02'
  get '/reserve03' => 'reserves#reserve03'
  post '/pay' => 'reserves#pay'

  resources :admins, only: [:index]
  get '/admin_store' => 'admins#admin_store'
  post '/admin_store_create' => 'admins#admin_store_create'
  get '/admin_license' => 'admins#admin_license'
  get '/admin_user' => 'admins#admin_user'
  patch '/role_change' => 'admins#role_change'
  patch '/store_change' => 'admins#store_change'
  get '/admin_store_edit' => 'admins#admin_store_edit'
  patch '/admin_store_update' => 'admins#admin_store_update'
  get '/admin_license_approval' => 'admins#admin_license_approval'
  get '/admin_reservation' => 'admins#admin_reservation'
  get '/admin_return_car' => 'admins#admin_return_car'

  resources :store_admins, only: [:index]
  get '/owner_entry' => 'store_admins#owner_entry'
  get '/owner_acceptance' => 'store_admins#owner_acceptance'
  get '/store_admin_edit' => 'store_admins#store_admin_edit'
  patch '/store_admin_update' => 'store_admins#store_admin_update'
  get '/owner_entry_infomation' => 'store_admins#owner_entry_infomation'
  post '/owner_approval' => 'store_admins#owner_approval'
  get '/owner_acceptance_car' => 'store_admins#owner_acceptance_car'
  get '/acceptance_car' => 'store_admins#acceptance_car'
  post '/owner_car_create' => 'store_admins#owner_car_create'
  get '/acceptance_car_edit' => 'store_admins#acceptance_car_edit'
  patch '/acceptance_car_update' => 'store_admins#acceptance_car_update'
  get '/store_admin_reservation' => 'store_admins#store_admin_reservation'
  get '/receipt_date_set' => 'store_admins#receipt_date_set'
  get '/store_return_car' => 'store_admins#store_return_car'
end
