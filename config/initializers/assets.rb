# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path.
# Rails.application.config.assets.paths << Emoji.images_path
# Add Yarn node_modules folder to the asset load path.
Rails.application.config.assets.paths << Rails.root.join('node_modules')

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in the app/assets
# folder are already added.
# Rails.application.config.assets.precompile += %w( admin.js admin.css )


#css
Rails.application.config.assets.precompile += %w( home/index )
Rails.application.config.assets.precompile += %w( home/about )
Rails.application.config.assets.precompile += %w( home/signup )
Rails.application.config.assets.precompile += %w( home/login )
Rails.application.config.assets.precompile += %w( home/news )
Rails.application.config.assets.precompile += %w( home/question )
Rails.application.config.assets.precompile += %w( home/search )
Rails.application.config.assets.precompile += %w( home/car01 )
Rails.application.config.assets.precompile += %w( home/car_search )
Rails.application.config.assets.precompile += %w( home/reserve01 )
Rails.application.config.assets.precompile += %w( home/reserve02 )
Rails.application.config.assets.precompile += %w( home/reserve03 )
Rails.application.config.assets.precompile += %w( home/reserved )
Rails.application.config.assets.precompile += %w( home/mypage )
Rails.application.config.assets.precompile += %w( home/ownerpage )
Rails.application.config.assets.precompile += %w( home/owner_consider )
Rails.application.config.assets.precompile += %w( home/owner_entry )
Rails.application.config.assets.precompile += %w( home/admin )
Rails.application.config.assets.precompile += %w( home/store_admin )
Rails.application.config.assets.precompile += %w( home/reserve_condition )
Rails.application.config.assets.precompile += %w( common/common )
Rails.application.config.assets.precompile += %w( common/footer )
Rails.application.config.assets.precompile += %w( common/header )
Rails.application.config.assets.precompile += %w( sticky-state.css )
Rails.application.config.assets.precompile += %w( drawer.css )
Rails.application.config.assets.precompile += %w( flickity.css )


#js
Rails.application.config.assets.precompile += %w( sticky-state.min.js )
Rails.application.config.assets.precompile += %w( sticky-state.js )
Rails.application.config.assets.precompile += %w( iscroll.js )
Rails.application.config.assets.precompile += %w( drawer.js )
Rails.application.config.assets.precompile += %w( flickity.pkgd.min.js )
