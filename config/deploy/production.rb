# EC2サーバーのIP、EC2サーバーにログインするユーザー名、サーバーのロールを記述
server '176.34.15.59', user: 'nakajima', roles: %w{app db web}

#デプロイするサーバーにsshログインする鍵の情報を記述
set :ssh_options, keys: '~/.ssh/ones_key_rsa'
