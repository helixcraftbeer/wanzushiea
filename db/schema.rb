# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20190404153405) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "accounts", force: :cascade do |t|
    t.integer  "owner_id"
    t.string   "financial_name"
    t.string   "branch_name"
    t.string   "deposit_category"
    t.string   "number"
    t.string   "holder"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  create_table "car_images", force: :cascade do |t|
    t.integer  "car_id"
    t.string   "image"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "car_options", force: :cascade do |t|
    t.integer  "car_id"
    t.integer  "option_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "cars", force: :cascade do |t|
    t.string   "name"
    t.string   "price"
    t.text     "description"
    t.string   "seat"
    t.integer  "store_id"
    t.integer  "category_id"
    t.integer  "owner_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "categories", force: :cascade do |t|
    t.string   "name"
    t.string   "uniq_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "cities", force: :cascade do |t|
    t.string   "name"
    t.string   "uniq_name"
    t.integer  "prefecture_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "information", force: :cascade do |t|
    t.date     "display_date"
    t.string   "title"
    t.text     "url"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "licenses", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "image"
    t.boolean  "approval",   default: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.integer  "owner_id"
  end

  create_table "options", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "owners", force: :cascade do |t|
    t.string   "name"
    t.string   "address"
    t.boolean  "approval",            default: false
    t.boolean  "acceptance",          default: false
    t.integer  "store_id"
    t.integer  "license_id"
    t.integer  "user_id"
    t.date     "birth_day"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.datetime "first_receipt_date"
    t.datetime "second_receipt_date"
    t.datetime "third_receipt_date"
    t.string   "receipt_date"
    t.string   "image"
    t.string   "tel"
    t.text     "description"
  end

  create_table "payments", force: :cascade do |t|
    t.integer  "user_id"
    t.text     "token"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "prefectures", force: :cascade do |t|
    t.string   "name"
    t.string   "uniq_name"
    t.string   "area_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "profiles", force: :cascade do |t|
    t.string   "name"
    t.integer  "user_id"
    t.date     "birth_day"
    t.string   "profession"
    t.string   "gender"
    t.string   "image"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "provisional_reservations", force: :cascade do |t|
    t.integer  "car_id"
    t.integer  "user_id"
    t.string   "state"
    t.integer  "number"
    t.integer  "insurance_fee"
    t.integer  "price"
    t.integer  "total"
    t.date     "start_date"
    t.date     "end_date"
    t.time     "start_time"
    t.time     "end_time"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "reservations", force: :cascade do |t|
    t.integer  "car_id"
    t.integer  "user_id"
    t.string   "state"
    t.integer  "number"
    t.integer  "insurance_fee"
    t.integer  "price"
    t.integer  "total"
    t.date     "start_date"
    t.date     "end_date"
    t.time     "start_time"
    t.time     "end_time"
    t.boolean  "payment"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "revenues", force: :cascade do |t|
    t.integer  "owner_id"
    t.integer  "price"
    t.integer  "reservation_id"
    t.date     "fixed_date"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "reviews", force: :cascade do |t|
    t.integer  "car_id"
    t.integer  "user_id"
    t.string   "title"
    t.string   "evaluation"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "roles", force: :cascade do |t|
    t.string   "name"
    t.string   "resource_type"
    t.integer  "resource_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id", using: :btree
    t.index ["resource_type", "resource_id"], name: "index_roles_on_resource_type_and_resource_id", using: :btree
  end

  create_table "store_images", force: :cascade do |t|
    t.integer  "store_id"
    t.string   "image"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "stores", force: :cascade do |t|
    t.string   "name"
    t.string   "tel"
    t.string   "image"
    t.text     "url"
    t.text     "access"
    t.string   "zip_code"
    t.text     "address"
    t.time     "business_hour_start"
    t.time     "business_hour_end"
    t.integer  "prefecture_id"
    t.integer  "city_id"
    t.float    "latitude"
    t.float    "longitude"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.text     "receipt_description"
    t.string   "email"
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "store_id"
    t.integer  "owner_id"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  end

  create_table "users_roles", id: false, force: :cascade do |t|
    t.integer "user_id"
    t.integer "role_id"
    t.index ["role_id"], name: "index_users_roles_on_role_id", using: :btree
    t.index ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id", using: :btree
    t.index ["user_id"], name: "index_users_roles_on_user_id", using: :btree
  end

end
