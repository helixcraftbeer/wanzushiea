class CreatePrefectures < ActiveRecord::Migration[5.0]
  def change
    create_table :prefectures do |t|

      t.string :name
      t.string :uniq_name
      t.string :area_name

      t.timestamps
    end
  end
end
