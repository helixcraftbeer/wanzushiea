class CreateReviews < ActiveRecord::Migration[5.0]
  def change
    create_table :reviews do |t|

      t.integer :car_id
      t.integer :user_id
      t.string :title
      t.string :evaluation
      t.text :description

      t.timestamps
    end
  end
end
