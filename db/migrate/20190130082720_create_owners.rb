class CreateOwners < ActiveRecord::Migration[5.0]
  def change
    create_table :owners do |t|

      t.string :name
      t.string :address
      t.boolean :approval, default: false
      t.boolean :acceptance, default: false
      t.integer :store_id
      t.integer :license_id
      t.integer :user_id
      t.date :birth_day

      t.timestamps
    end
  end
end
