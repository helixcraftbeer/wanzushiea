class CreateAccounts < ActiveRecord::Migration[5.0]
  def change
    create_table :accounts do |t|

      t.integer :owner_id
      t.string :financial_name
      t.string :branch_name
      t.string :deposit_category
      t.string :number
      t.string :holder
      t.timestamps
    end
  end
end
