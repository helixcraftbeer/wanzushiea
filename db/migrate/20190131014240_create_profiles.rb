class CreateProfiles < ActiveRecord::Migration[5.0]
  def change
    create_table :profiles do |t|

      t.string :name
      t.integer :user_id
      t.date :birth_day
      t.string :profession
      t.string :gender
      t.string :image

      t.timestamps
    end
  end
end
