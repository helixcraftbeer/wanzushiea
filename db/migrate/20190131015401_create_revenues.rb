class CreateRevenues < ActiveRecord::Migration[5.0]
  def change
    create_table :revenues do |t|

      t.integer :owner_id
      t.integer :price
      t.integer :reservation_id
      t.date :fixed_date
      t.timestamps
    end
  end
end
