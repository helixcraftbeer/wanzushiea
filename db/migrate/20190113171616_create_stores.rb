class CreateStores < ActiveRecord::Migration[5.0]
  def change
    create_table :stores do |t|

      t.string :name
      t.string :tel
      t.string :image
      t.text :url
      t.text :access
      t.string :zip_code
      t.text :address
      t.time :business_hour_start
      t.time :business_hour_end
      t.integer :prefecture_id
      t.integer :city_id
      t.float :latitude
      t.float :longitude

      t.timestamps
    end
  end
end
