class AddColumnLicense < ActiveRecord::Migration[5.0]
  def change
    add_column :licenses, :owner_id, :integer
  end
end
