class CreateCars < ActiveRecord::Migration[5.0]
  def change
    create_table :cars do |t|

      t.string :name
      t.string :price
      t.text :description
      t.string :seat
      t.integer :store_id
      t.integer :category_id
      t.integer :owner_id

      t.timestamps
    end
  end
end
