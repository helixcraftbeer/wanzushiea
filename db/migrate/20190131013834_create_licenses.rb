class CreateLicenses < ActiveRecord::Migration[5.0]
  def change
    create_table :licenses do |t|

        t.integer :user_id
        t.string :image
        t.boolean :approval, default: false

      t.timestamps
    end
  end
end
