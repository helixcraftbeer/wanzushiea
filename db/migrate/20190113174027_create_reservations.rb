class CreateReservations < ActiveRecord::Migration[5.0]
  def change
    create_table :reservations do |t|

      t.integer :car_id
      t.integer :user_id
      t.string :state
      t.integer :number
      t.integer :insurance_fee
      t.integer :price
      t.integer :total
      t.date :start_date
      t.date :end_date
      t.time :start_time
      t.time :end_time
      t.boolean :payment

      t.timestamps
    end
  end
end
