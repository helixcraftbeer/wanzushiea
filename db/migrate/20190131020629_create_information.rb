class CreateInformation < ActiveRecord::Migration[5.0]
  def change
    create_table :information do |t|

      t.date :display_date
      t.string :title
      t.text :url

      t.timestamps
    end
  end
end
