class AddColumnStores < ActiveRecord::Migration[5.0]
  def change
    add_column :stores, :receipt_description, :text
  end
end
