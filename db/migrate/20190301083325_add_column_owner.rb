class AddColumnOwner < ActiveRecord::Migration[5.0]
  def change
    add_column :owners, :first_receipt_date, :datetime
    add_column :owners, :second_receipt_date, :datetime
    add_column :owners, :third_receipt_date, :datetime
  end
end
