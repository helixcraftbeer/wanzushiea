class CreateCarOptions < ActiveRecord::Migration[5.0]
  def change
    create_table :car_options do |t|

      t.integer :car_id
      t.integer :option_id

      t.timestamps
    end
  end
end
