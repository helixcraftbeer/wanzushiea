# == Schema Information
#
# Table name: reviews
#
#  id          :integer          not null, primary key
#  car_id      :integer
#  user_id     :integer
#  title       :string
#  evaluation  :string
#  description :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Review < ApplicationRecord
  belongs_to :car
  belongs_to :user
end
