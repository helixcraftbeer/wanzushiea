# == Schema Information
#
# Table name: information
#
#  id           :integer          not null, primary key
#  display_date :date
#  title        :string
#  url          :text
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class Information < ApplicationRecord
end
