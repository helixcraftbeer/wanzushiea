# == Schema Information
#
# Table name: options
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Option < ApplicationRecord
  has_many :car_options
  has_many :cars, through: :car_categories
end
