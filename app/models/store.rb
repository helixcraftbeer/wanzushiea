# == Schema Information
#
# Table name: stores
#
#  id                  :integer          not null, primary key
#  name                :string
#  tel                 :string
#  image               :string
#  url                 :text
#  access              :text
#  zip_code            :string
#  address             :text
#  business_hour_start :time
#  business_hour_end   :time
#  prefecture_id       :integer
#  city_id             :integer
#  latitude            :float
#  longitude           :float
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  receipt_description :text
#  email               :string
#

class Store < ApplicationRecord
  has_many :cars
  belongs_to :prefecture
  belongs_to :city
  mount_uploader :image, ImageUploader

  # geocoded_by :address
  # after_validation :geocode
  #
  # def address
  #   lati_long = Geocoder.coordinates(address)
  #   self.latitude = lati_long[0]
  #   self.longitude = lati_long[1]
  # end

end
