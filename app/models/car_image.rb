# == Schema Information
#
# Table name: car_images
#
#  id         :integer          not null, primary key
#  car_id     :integer
#  image      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class CarImage < ApplicationRecord
  mount_uploader :image, ImageUploader
  belongs_to :car
end
