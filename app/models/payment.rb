# == Schema Information
#
# Table name: payments
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  token      :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Payment < ApplicationRecord
  belongs_to :user
  attr_accessor :number
  attr_accessor :cvc
  attr_accessor :exp_month
  attr_accessor :exp_year

  require 'payjp'

  Payjp::api_key = "sk_test_3c4fcc6d32f9a15baaac339f"

  def self.create_token(number, cvc, exp_month, exp_year)
    Payjp::api_key = "sk_test_3c4fcc6d32f9a15baaac339f"
    token = Payjp::Token.create(
      card: {
        number:    number,
        cvc:       cvc,
        exp_year:  exp_year,
        exp_month: exp_month,
      }
    )
    return token.id
  end

  def self.create_charge_by_token(token, amount)
    Payjp::api_key = "sk_test_3c4fcc6d32f9a15baaac339f"
    Payjp::Charge.create(
      amount:   amount,
      card:     token,
      currency: 'jpy'
    )
  end

  def self.create_customer(number, cvc, exp_month, exp_year)
    Payjp::api_key = "sk_test_3c4fcc6d32f9a15baaac339f"
    token = self.create_token(number, cvc, exp_month, exp_year)
    Payjp::Customer.create(card: token)
  end

  def self.create_charge_by_customer(customer, amount)
    Payjp::api_key = "sk_test_3c4fcc6d32f9a15baaac339f"
    Payjp::Charge.create(
      amount:   amount,
      customer: customer,
      currency: 'jpy'
    )
  end

end
