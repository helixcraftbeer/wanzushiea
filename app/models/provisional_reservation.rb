# == Schema Information
#
# Table name: provisional_reservations
#
#  id            :integer          not null, primary key
#  car_id        :integer
#  user_id       :integer
#  state         :string
#  number        :integer
#  insurance_fee :integer
#  price         :integer
#  total         :integer
#  start_date    :date
#  end_date      :date
#  start_time    :time
#  end_time      :time
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  option_ids    :integer[]
#  option_price  :integer
#

class ProvisionalReservation < ApplicationRecord
  belongs_to :car
  belongs_to :user

    def self.insurance_fee_calculation number
      num = number.to_i
      insurance_fee = num * 1000
      return insurance_fee
    end

    def self.option_price_calculation option_ids, start_date, start_time, end_date, end_time, business_hour_start, business_hour_end
      if business_hour_start.hour <= start_time.hour
        if start_time.hour <= business_hour_end.hour
          within = true
        else
          within = false
        end
      else
        within = false
      end
      st = DateTime.new(start_date.year, start_date.month, start_date.day, start_time.hour, start_time.min)
      en = DateTime.new(end_date.year, end_date.month, end_date.day, end_time.hour, end_time.min)
      difference = ((en - st) * 24).to_i
      tm_option_price = option_ids.compact
      result_price = 0
      _ = tm_option_price.map do |option|
        if option == 1
          ## ETC
          if difference == 1
            result_price += 540
          elsif within && difference < 24
            result_price += 540
          elsif difference <= 24
            result_price += 540
          elsif within && difference < 48 && 24 < difference
            result_price += 1080
          elsif difference <= 48
            result_price += 1080
          elsif difference == 696
            result_price += 1620
          elsif difference == 720
            result_price += 2160
          elsif difference == 744
            result_price += 2700
          else
            a = difference - 48
            b = a / 24
            if b == 0
              result_price += 2700
            else
              result_price += 2700 + b * 1080
            end
          end
        end

        if option == 2
          ##
          if difference == 1
            result_price += 540
          elsif within && difference < 24
            result_price += 540
          elsif difference <= 24
            result_price += 540
          elsif within && difference < 48 && 24 < difference
            result_price += 1080
          elsif difference <= 48
            result_price += 1080
          elsif difference == 696
            result_price += 1620
          elsif difference == 720
            result_price += 2160
          elsif difference == 744
            result_price += 2700
          else
            a = difference - 48
            b = a / 24
            if b == 0
              result_price += 2700
            else
              result_price += 2700 + b * 1080
            end
          end
        end

        if option == 3
          ##
          if difference == 1
            result_price += 540
          elsif within && difference < 24
            result_price += 540
          elsif difference <= 24
            result_price += 540
          elsif within && difference < 48 && 24 < difference
            result_price += 1080
          elsif difference <= 48
            result_price += 1080
          elsif difference == 696
            result_price += 1620
          elsif difference == 720
            result_price += 2160
          elsif difference == 744
            result_price += 2700
          else
            a = difference - 48
            b = a / 24
            if b == 0
              result_price += 2700
            else
              result_price += 2700 + b * 1080
            end
          end
        end

        if option == 4
          ##
          if difference == 1
            result_price += 540
          elsif within && difference < 24
            result_price += 540
          elsif difference <= 24
            result_price += 540
          elsif within && difference < 48 && 24 < difference
            result_price += 1080
          elsif difference <= 48
            result_price += 1080
          elsif difference == 696
            result_price += 1620
          elsif difference == 720
            result_price += 2160
          elsif difference == 744
            result_price += 2700
          else
            a = difference - 48
            b = a / 24
            if b == 0
              result_price += 2700
            else
              result_price += 2700 + b * 1080
            end
          end
        end

        if option == 5
          ##
          if difference == 1
            result_price += 540
          elsif within && difference < 24
            result_price += 540
          elsif difference <= 24
            result_price += 540
          elsif within && difference < 48 && 24 < difference
            result_price += 1080
          elsif difference <= 48
            result_price += 1080
          elsif difference == 696
            result_price += 1620
          elsif difference == 720
            result_price += 2160
          elsif difference == 744
            result_price += 2700
          else
            a = difference - 48
            b = a / 24
            if b == 0
              result_price += 2700
            else
              result_price += 2700 + b * 1080
            end
          end
        end

        if option == 6
          ##
          if difference == 1
            result_price += 540
          elsif within && difference < 24
            result_price += 540
          elsif difference <= 24
            result_price += 540
          elsif within && difference < 48 && 24 < difference
            result_price += 1080
          elsif difference <= 48
            result_price += 1080
          elsif difference == 696
            result_price += 1620
          elsif difference == 720
            result_price += 2160
          elsif difference == 744
            result_price += 2700
          else
            a = difference - 48
            b = a / 24
            if b == 0
              result_price += 2700
            else
              result_price += 2700 + b * 1080
            end
          end
        end

        if option == 7
          ##
          if difference == 1
            result_price += 540
          elsif within && difference < 24
            result_price += 540
          elsif difference <= 24
            result_price += 540
          elsif within && difference < 48 && 24 < difference
            result_price += 1080
          elsif difference <= 48
            result_price += 1080
          elsif difference == 696
            result_price += 1620
          elsif difference == 720
            result_price += 2160
          elsif difference == 744
            result_price += 2700
          else
            a = difference - 48
            b = a / 24
            if b == 0
              result_price += 2700
            else
              result_price += 2700 + b * 1080
            end
          end
        end

        if option == 8
          ##
          if difference == 1
            result_price += 540
          elsif within && difference < 24
            result_price += 540
          elsif difference <= 24
            result_price += 540
          elsif within && difference < 48 && 24 < difference
            result_price += 1080
          elsif difference <= 48
            result_price += 1080
          elsif difference == 696
            result_price += 1620
          elsif difference == 720
            result_price += 2160
          elsif difference == 744
            result_price += 2700
          else
            a = difference - 48
            b = a / 24
            if b == 0
              result_price += 2700
            else
              result_price += 2700 + b * 1080
            end
          end
        end

        if option == 9
          ##
          if difference == 1
            result_price += 540
          elsif within && difference < 24
            result_price += 540
          elsif difference <= 24
            result_price += 540
          elsif within && difference < 48 && 24 < difference
            result_price += 1080
          elsif difference <= 48
            result_price += 1080
          elsif difference == 696
            result_price += 1620
          elsif difference == 720
            result_price += 2160
          elsif difference == 744
            result_price += 2700
          else
            a = difference - 48
            b = a / 24
            if b == 0
              result_price += 2700
            else
              result_price += 2700 + b * 1080
            end
          end
        end

        if option == 10
          ##
          if difference == 1
            result_price += 540
          elsif within && difference < 24
            result_price += 540
          elsif difference <= 24
            result_price += 540
          elsif within && difference < 48 && 24 < difference
            result_price += 1080
          elsif difference <= 48
            result_price += 1080
          elsif difference == 696
            result_price += 1620
          elsif difference == 720
            result_price += 2160
          elsif difference == 744
            result_price += 2700
          else
            a = difference - 48
            b = a / 24
            if b == 0
              result_price += 2700
            else
              result_price += 2700 + b * 1080
            end
          end
        end

        if option == 11
          ##
          if difference == 1
            result_price += 540
          elsif within && difference < 24
            result_price += 540
          elsif difference <= 24
            result_price += 540
          elsif within && difference < 48 && 24 < difference
            result_price += 1080
          elsif difference <= 48
            result_price += 1080
          elsif difference == 696
            result_price += 1620
          elsif difference == 720
            result_price += 2160
          elsif difference == 744
            result_price += 2700
          else
            a = difference - 48
            b = a / 24
            if b == 0
              result_price += 2700
            else
              result_price += 2700 + b * 1080
            end
          end
        end

      end

      option_price = result_price
      return option_price
    end

    def self.price_calculation start_date, start_time, end_date, end_time, category_id, business_hour_start, business_hour_end
      if business_hour_start.hour <= start_time.hour
         if start_time.hour <= business_hour_end.hour
           within = true
         else
           within = false
         end
      else
        within = false
      end
      st = DateTime.new(start_date.year, start_date.month, start_date.day, start_time.hour, start_time.min)
      en = DateTime.new(end_date.year, end_date.month, end_date.day, end_time.hour, end_time.min)
      difference = ((en - st) * 24).to_i
      if category_id == 1 || category_id == 2
        ## M M(軽)
        if difference == 1
          price = 1080
        elsif within && difference < 24
          price = 2700
        elsif difference <= 24
          price = 3780
        elsif within && difference < 48 && 24 < difference
          price = 6480
        elsif difference <= 48
          price = 7560
        elsif difference == 696
          price = 75600
        elsif difference == 720
          price = 75600
        elsif difference == 744
          price = 75600
        else
          a = difference - 48
          b = a / 24
          if b == 0
            price = 7560
          else
            price = 7560 + b * 3780
          end
        end
      elsif category_id == 3
        ## B
        if difference == 1
          price = 1188
        elsif within && difference < 24
          price = 4320
        elsif difference <= 24
          price = 4860
        elsif within && difference < 48 && 24 < difference
          price = 9180
        elsif difference <= 48
          price = 9720
        elsif difference == 696
          price = 108000
        elsif difference == 720
          price = 108000
        elsif difference == 744
          price = 108000
        else
          a = difference - 48
          b = a / 24
          if b == 0
            price = 9720
          else
            price = 9720 + b * 4860
          end
        end
      elsif category_id == 4
        # L
        if difference == 1
          price = 1296
        elsif within && difference < 24
          price = 5400
        elsif difference <= 24
          price = 6480
        elsif within && difference < 48 && 24 < difference
          price = 11880
        elsif difference <= 48
          price = 12960
        elsif difference == 696
          price = 129600
        elsif difference == 720
          price = 129600
        elsif difference == 744
          price = 129600
        else
          a = difference - 48
          b = a / 24
          if b == 0
            price = 12960
          else
            price = 12960 + b * 6480
          end
        end
      elsif category_id == 5
        #1BOX
        if difference == 1
          price = 1620
        elsif within && difference < 24
          price = 7560
        elsif difference <= 24
          price = 8640
        elsif within && difference < 48 && 24 < difference
          price = 16200
        elsif difference <= 48
          price = 17280
        elsif difference == 696
          price = 162000
        elsif difference == 720
          price = 162000
        elsif difference == 744
          price = 162000
        else
          a = difference - 48
          b = a / 24
          if b == 0
            price = 17280
          else
            price = 17280 + b * 8640
          end
        end
      elsif category_id == 6
        #HV
        if difference == 1
          price = 1296
        elsif within && difference < 24
          price = 5400
        elsif difference <= 24
          price = 6480
        elsif within && difference < 48 && 24 < difference
          price = 11880
        elsif difference <= 48
          price = 12960
        elsif difference == 696
          price = 129600
        elsif difference == 720
          price = 129600
        elsif difference == 744
          price = 129600
        else
          a = difference - 48
          b = a / 24
          if b == 0
            price = 12960
          else
            price = 12960 + b * 6480
          end
        end
      elsif category_id == 7 || category_id == 8
        #M-SP M(軽)-SP
        if difference == 1
          price = 1728
        elsif within && difference < 24
          price = 4320
        elsif difference <= 24
          price = 4860
        elsif within && difference < 48 && 24 < difference
          price = 9180
        elsif difference <= 48
          price = 9720
        elsif difference == 696
          price = 97200
        elsif difference == 720
          price = 97200
        elsif difference == 744
          price = 97200
        else
          a = difference - 48
          b = a / 24
          if b == 0
            price = 9720
          else
            price = 9720 + b * 4860
          end
        end
      elsif category_id == 9
        #B-SP
        if difference == 1
          price = 1836
        elsif within && difference < 24
          price = 5400
        elsif difference <= 24
          price = 7020
        elsif within && difference < 48 && 24 < difference
          price = 12420
        elsif difference <= 48
          price = 14040
        elsif difference == 696
          price = 129600
        elsif difference == 720
          price = 129600
        elsif difference == 744
          price = 129600
        else
          a = difference - 48
          b = a / 24
          if b == 0
            price = 14040
          else
            price = 14040 + b * 7020
          end
        end
      elsif category_id == 10
        #L-SP
        if difference == 1
          price = 1944
        elsif within && difference < 24
          price = 7020
        elsif difference <= 24
          price = 8640
        elsif within && difference < 48 && 24 < difference
          price = 15660
        elsif difference <= 48
          price = 17280
        elsif difference == 696
          price = 151200
        elsif difference == 720
          price = 151200
        elsif difference == 744
          price = 151200
        else
          a = difference - 48
          b = a / 24
          if b == 0
            price = 17280
          else
            price = 17280 + b * 8640
          end
        end
      elsif category_id == 11
        #1BOX-SP
        if difference == 1
          price = 2160
        elsif within && difference < 24
          price = 9180
        elsif difference <= 24
          price = 11880
        elsif within && difference < 48 && 24 < difference
          price = 21060
        elsif difference <= 48
          price = 23760
        elsif difference == 696
          price = 183600
        elsif difference == 720
          price = 183600
        elsif difference == 744
          price = 183600
        else
          a = difference - 48
          b = a / 24
          if b == 0
            price = 23760
          else
            price = 23760 + b * 11880
          end
        end
      elsif category_id == 12
        #HV-SP
        if difference == 1
          price = 1944
        elsif within && difference < 24
          price = 7020
        elsif difference <= 24
          price = 9180
        elsif within && difference < 48 && 24 < difference
          price = 16200
        elsif difference <= 48
          price = 18360
        elsif difference == 696
          price = 151200
        elsif difference == 720
          price = 151200
        elsif difference == 744
          price = 151200
        else
          a = difference - 48
          b = a / 24
          if b == 0
            price = 18360
          else
            price = 18360 + b * 9180
          end
        end
      elsif category_id == 13 || category_id == 14
        #T1 T1(軽)
        if difference == 1
          price = 1296
        elsif within && difference < 24
          price = 4320
        elsif difference <= 24
          price = 5400
        elsif within && difference < 48 && 24 < difference
          price = 9720
        elsif difference <= 48
          price = 10800
        elsif difference == 696
          price = 129600
        elsif difference == 720
          price = 129600
        elsif difference == 744
          price = 129600
        else
          a = difference - 48
          b = a / 24
          if b == 0
            price = 10800
          else
            price = 10800 + b * 5400
          end
        end
      elsif category_id == 15
        #T1-SP
        if difference == 1
          price = 1944
        elsif within && difference < 24
          price = 5940
        elsif difference <= 24
          price = 7020
        elsif within && difference < 48 && 24 < difference
          price = 12960
        elsif difference <= 48
          price = 14040
        elsif difference == 696
          price = 151200
        elsif difference == 720
          price = 151200
        elsif difference == 744
          price = 151200
        else
          a = difference - 48
          b = a / 24
          if b == 0
            price = 14040
          else
            price = 14040 + b * 7020
          end
        end
      elsif category_id == 16
        #T2
        if difference == 1
          price = 1620
        elsif within && difference < 24
          price = 7020
        elsif difference <= 24
          price = 9180
        elsif within && difference < 48 && 24 < difference
          price = 16200
        elsif difference <= 48
          price = 18360
        elsif difference == 696
          price = 162000
        elsif difference == 720
          price = 16200
        elsif difference == 744
          price = 16200
        else
          a = difference - 48
          b = a / 24
          if b == 0
            price = 18360
          else
            price = 18360 + b * 9180
          end
        end
      elsif category_id == 17
        #T3
        if difference == 1
          price = 2484
        elsif within && difference < 24
          price = 11340
        elsif difference <= 24
          price = 15120
        elsif within && difference < 48 && 24 < difference
          price = 26460
        elsif difference <= 48
          price = 30240
        elsif difference == 696
          price = 194400
        elsif difference == 720
          price = 194400
        elsif difference == 744
          price = 194400
        else
          a = difference - 48
          b = a / 24
          if b == 0
            price = 30240
          else
            price = 30240 + b * 15120
          end
        end
      end
      return price
    end

end
