# == Schema Information
#
# Table name: reservations
#
#  id            :integer          not null, primary key
#  car_id        :integer
#  user_id       :integer
#  state         :string
#  number        :integer
#  insurance_fee :integer
#  price         :integer
#  total         :integer
#  start_date    :date
#  end_date      :date
#  start_time    :time
#  end_time      :time
#  payment       :boolean
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class Reservation < ApplicationRecord
  belongs_to :car
  belongs_to :user

  def self.check_reserved car_id, pro_res_id
    pro_res = ProvisionalReservation.find(pro_res_id)
    reservations = Reservation.where(car_id: car_id)
    reservations_check = []
    reservations.each do |res|
      res_start = DateTime.new(res.start_date.year, res.start_date.month, res.start_date.day, res.start_time.hour, res.start_time.min)
      res_end = DateTime.new(res.end_date.year, res.end_date.month, res.end_date.day, res.end_time.hour, res.end_time.min)
      pro_res_start = DateTime.new(pro_res.start_date.year, pro_res.start_date.month, pro_res.start_date.day, pro_res.start_time.hour, pro_res.start_time.min)
      pro_res_end = DateTime.new(pro_res.end_date.year, pro_res.end_date.month, pro_res.end_date.day, pro_res.end_time.hour, pro_res.end_time.min)
      if res_start <= pro_res_start
        if pro_res_start <= res_end + 1.hour
          reservations_check << res.id
        end
      else
        if res_start <= pro_res_end + 1.hour
          reservations_check << res.id
        end
      end
    end
    if reservations_check.present?
      return reservations_check[0]
    else
      return nil
    end
  end

end
