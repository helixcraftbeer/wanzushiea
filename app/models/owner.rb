# == Schema Information
#
# Table name: owners
#
#  id                  :integer          not null, primary key
#  name                :string
#  address             :string
#  approval            :boolean          default(FALSE)
#  acceptance          :boolean          default(FALSE)
#  store_id            :integer
#  license_id          :integer
#  user_id             :integer
#  birth_day           :date
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  first_receipt_date  :datetime
#  second_receipt_date :datetime
#  third_receipt_date  :datetime
#  receipt_date        :string
#  image               :string
#  tel                 :string
#  description         :text
#

class Owner < ApplicationRecord
  attr_accessor :car_name
  attr_accessor :category_id
  mount_uploader :image, ImageUploader
  has_one :license
  accepts_nested_attributes_for :license
  belongs_to :store
  belongs_to :user
end
