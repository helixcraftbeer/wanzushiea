# == Schema Information
#
# Table name: car_options
#
#  id         :integer          not null, primary key
#  car_id     :integer
#  option_id  :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class CarOption < ApplicationRecord
  belongs_to :car
  belongs_to :option
end
