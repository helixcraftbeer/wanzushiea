# == Schema Information
#
# Table name: cities
#
#  id            :integer          not null, primary key
#  name          :string
#  uniq_name     :string
#  prefecture_id :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class City < ApplicationRecord
  belongs_to :prefecture
end
