# == Schema Information
#
# Table name: profiles
#
#  id         :integer          not null, primary key
#  name       :string
#  user_id    :integer
#  birth_day  :date
#  profession :string
#  gender     :string
#  image      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Profile < ApplicationRecord
  mount_uploader :image, ImageUploader
  belongs_to :user

  def age
    date_format = "%Y%m%d"
    (Date.today.strftime(date_format).to_i - birth_day.strftime(date_format).to_i) / 10000
  end
end
