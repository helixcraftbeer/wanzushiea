# == Schema Information
#
# Table name: revenues
#
#  id             :integer          not null, primary key
#  owner_id       :integer
#  price          :integer
#  reservation_id :integer
#  fixed_date     :date
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class Revenue < ApplicationRecord
end
