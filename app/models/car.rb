# == Schema Information
#
# Table name: cars
#
#  id          :integer          not null, primary key
#  name        :string
#  price       :string
#  description :text
#  seat        :string
#  store_id    :integer
#  category_id :integer
#  owner_id    :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Car < ApplicationRecord
  belongs_to :store
  belongs_to :category
  belongs_to :owner
  has_many :car_images
  accepts_nested_attributes_for :car_images
  has_many :reviews
  has_many :car_options
  has_many :options, through: :car_options
  accepts_nested_attributes_for :car_options, allow_destroy: true

  scope :refine_by, -> (option, category){
    if option
      joins(:options).where(:options => {id: option}).uniq
    end
    if category
      joins(:category).where(:categories => {id: category}).uniq
    end
  }

  def price_set
    if category_id == 1 || category_id == 2
      self.update(price: 1080)
    elsif category_id == 3
      self.update(price: 1188)
    elsif category_id == 4
      self.update(price: 1296)
    elsif category_id == 5
      self.update(price: 1620)
    elsif category_id == 6
      self.update(price: 1296)
    elsif category_id == 7 || category_id == 8
      self.update(price: 1728)
    elsif category_id == 9
      self.update(price: 1836)
    elsif category_id == 10
      self.update(price: 1944)
    elsif category_id == 11
      self.update(price: 2160)
    elsif category_id == 12
      self.update(price: 1944)
    elsif category_id == 13 || category_id == 14
      self.update(price: 1296)
    elsif category_id == 15
      self.update(price: 1944)
    elsif category_id == 16
      self.update(price: 1620)
    elsif category_id == 17
      self.update(price: 2484)
    end
  end

end
