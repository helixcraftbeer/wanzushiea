# == Schema Information
#
# Table name: store_images
#
#  id         :integer          not null, primary key
#  store_id   :integer
#  image      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class StoreImage < ApplicationRecord
  mount_uploader :image, ImageUploader
end
