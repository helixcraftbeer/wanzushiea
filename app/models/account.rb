# == Schema Information
#
# Table name: accounts
#
#  id               :integer          not null, primary key
#  owner_id         :integer
#  financial_name   :string
#  branch_name      :string
#  deposit_category :string
#  number           :string
#  holder           :string
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

class Account < ApplicationRecord
  belongs_to :owner
end
