# == Schema Information
#
# Table name: prefectures
#
#  id         :integer          not null, primary key
#  name       :string
#  uniq_name  :string
#  area_name  :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Prefecture < ApplicationRecord
  has_many :cities
end
