# == Schema Information
#
# Table name: licenses
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  image      :string
#  approval   :boolean          default(FALSE)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  owner_id   :integer
#

class License < ApplicationRecord
  mount_uploader :image, ImageUploader
  belongs_to :user
end
