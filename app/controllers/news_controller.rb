class NewsController < ApplicationController

  def index
    @informations = Information.order(created_at: :desc).page(params[:page]).per(5)
  end
end
