class StoreAdminsController < ApplicationController

  def index
    @store = current_user.try(:store)
  end

  def owner_entry
    @store = current_user.try(:store)
    @owners = Owner.where(store_id: @store.id, approval: false)
  end

  def owner_acceptance
    @store = current_user.try(:store)
    @owners = Owner.where(store_id: @store.id, approval: true, acceptance: false)
  end

  def store_admin_edit
    @store = current_user.try(:store)
  end

  def owner_entry_infomation
    @store = current_user.try(:store)
    @owner = Owner.find(params[:id])
  end

  def store_admin_update
    store = Store.find(params[:id])
    store.update(store_params)
    redirect_to store_admins_path
  end

  def owner_approval
    owner = Owner.find(params[:id])
    store = current_user.try(:store)
    owner.update(approval: true, receipt_date: params[:receipt_date])
    OwnerEntryMailer.owner_notification(owner, store).deliver_now
  end

  def owner_acceptance_car
    @owner = Owner.find(params[:id])
    @store = current_user.try(:store)
  end

  def acceptance_car
    @store = current_user.try(:store)
    store = current_user.try(:store)
    @cars = Car.where(store_id: store.id)
  end

  def acceptance_car_edit
    @store = current_user.try(:store)
    @car = Car.find(params[:id])
  end

  def acceptance_car_update
    @car = Car.find(params[:id])
    @car.update(car_params)
    redirect_to acceptance_car_path
  end

  def owner_car_create
    car = Car.new(car_params)
    if car.save
      car.price_set
      owner = Owner.find(car.owner_id)
      owner.update(acceptance: true)
      redirect_to acceptance_car_path
    end
  end

  def store_admin_reservation
    @store = current_user.try(:store)
    @reservations = Reservation.joins(:car).where(:cars => {store_id: @store.id}).page(params[:page]).per(10)
  end

  def receipt_date_set
    @store = current_user.try(:store)
  end

  def store_return_car
    reservation = Reservation.find(params[:res_id])
    if params[:tab] == "rental"
      reservation.update(state: "rental")
    elsif params[:tab] == "returned"
      reservation.update(state: "returned")
      CarReturnMailer.returned_notification(reservation.user, reservation.car).deliver_now
    end
    redirect_to store_admin_reservation_path
  end

  private
  def store_params
    params.require(:store).permit!
  end

  def car_params
    params.require(:car).permit!
  end
end
