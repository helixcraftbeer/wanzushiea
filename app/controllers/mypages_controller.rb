class MypagesController < ApplicationController

  def index
    @reservations = Reservation.where(user_id: current_user.id)
  end

  def profile
    if current_user.profile
      @profile = current_user.profile
    end
  end

  def profile_create
    profile = Profile.new(profile_params)
    profile.save
    redirect_to profile_path
  end

  def profile_update
    profile = Profile.find(params[:id])
    profile.update(profile_params)
    redirect_to profile_path
  end

  def license
    if current_user.license
      @license = current_user.license
    end
  end

  def license_create
    license = License.new(license_params)
    license.save
    redirect_to license_path
  end

  def license_update
    license = License.find(params[:id])
    license.update(license_params)
    redirect_to license_path
  end

  def review
  end

  def review_create
    review = Review.new(review_params)
    review.save
    redirect_to mypages_path
  end

  def notification
  end

  def notification_set
  end

  def payment
  end

  def privacy
  end

  def password
  end

  def password_set
  end

  def detail
  end

  def detail_set
  end

  private

  def profile_params
    params.require(:profile).permit!
  end

  def license_params
    params.require(:license).permit!
  end

  def review_params
    params.require(:review).permit!
  end


end
