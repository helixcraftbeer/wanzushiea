class ReserveConditionsController < ApplicationController

  def index
    @reservation = Reservation.find_by(user_id: current_user.id, id: params[:res_id])
    @car = @reservation.car
    @store = @car.store
    @hash = [{:lat=>@store.latitude, :lng=>@store.longitude}]
  end
end
