class CarSearchsController < ApplicationController

  def index
    if params[:store_id].present?
      @store = Store.find(params[:store_id])
    elsif params[:pre].present?
      @prefecture = Prefecture.find_by(uniq_name: params[:pre])
    elsif params[:cat].present?
      @category = Category.find_by(uniq_name: params[:cat])
    end

    if @store.present?
      @cars = Car.where(store_id: @store.id)
      prefecture = @store.prefecture
      @places = Store.where(prefecture_id: prefecture.id)
    elsif @prefecture.present?
      @cars = Car.joins(:store).where(:stores => {prefecture_id: @prefecture.id})
      @places = Store.where(prefecture_id: @prefecture.id)
    elsif @category.present?
      @cars = Car.joins(:categories).where(:categories => {id: @category.id})
    end

    @options = Option.all
    @categories = Category.all
    @prefectures = Prefecture.all

    @cars = @cars.refine_by(params[:option], params[:category])

    if params[:start_times]
      @cars = reserved_car(params[:start_times], params[:end_times], @cars)
    end

    @option = true if params[:option]
    @category = true if params[:category]

    unless @places.present?
      @places = Store.last
    end

    @latitude = @places.first.latitude
    @longitude = @places.first.longitude

    @hash = Gmaps4rails.build_markers(@places) do |place, marker|
      marker.lat place.latitude
      marker.lng place.longitude
      marker.infowindow render_to_string( partial: "infowindow",
                                          locals: { place: place} )
      marker.json({ id: place.id, })
    end

  end

  def detail_search
    url = car_searchs_url(pre: params[:pre], option: params[:option], category: params[:category], start_times: params[:start_times], end_times: params[:end_times])
    render json: url if request.xhr?
  end

  def reserved_car start_times, end_times, cars
    start_date = Date.new(start_times[0].to_i, start_times[1].to_i, start_times[2].to_i)
    start_time = DateTime.new(start_times[0].to_i, start_times[1].to_i, start_times[2].to_i, start_times[3].to_i, start_times[4].to_i)
    end_time = DateTime.new(end_times[0].to_i, end_times[1].to_i, end_times[2].to_i, end_times[3].to_i, end_times[4].to_i)
    reservations = Reservation.where('start_date >= ?', start_date)
    ids = []
    reservations.each do |res|
      res_start = DateTime.new(res.start_date.year, res.start_date.month, res.start_date.day, res.start_time.hour, res.start_time.min)
      res_end = DateTime.new(res.end_date.year, res.end_date.month, res.end_date.day, res.end_time.hour, res.end_time.min)
      if res_start <= start_time
        if start_time <= res_end + 1.hour
          ids << res.id
        end
      else
        if res_start <= end_time + 1.hour
          ids << res.id
        end
      end
    end
    return cars.where.not(id: ids)
  end

end
