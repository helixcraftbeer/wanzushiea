class OwnerpagesController < ApplicationController

  def index
    @owner = current_user.try(:owner)
    if @owner
      unless @owner.approval
        redirect_to not_approval_path
        return
      end
      unless @owner.acceptance
        redirect_to not_acceptance_path
        return
      end
      @cars = Car.where(owner_id: @owner.id)
    else
      redirect_to entry_path
      return
    end
  end

  def performance
    @owner = current_user.try(:owner)
    @reservations = Reservation.joins(:car).where(:cars => {owner_id: @owner.id})
  end

  def revenue
    @owner = current_user.try(:owner)
    @reservations = Reservation.where(start_date: Time.now.all_month).joins(:car).where(:cars => {owner_id: @owner.id})
    @total = 0
    @reservations.each do |re|
      @total += re.total
    end
  end

  def consider
  end

  def entry
  end

  def not_approval
  end

  def not_acceptance
    @owner = current_user.try(:owner)
    @store = @owner.store
    @hash = [{:lat=>@store.latitude, :lng=>@store.longitude}]
  end

  def car_edit
    @owner = current_user.try(:owner)
    @car = Car.find_by(id: params[:car_id], owner_id: @owner.id)
  end

  def car_update
  end

  def owner_profile
    @owner = current_user.try(:owner)
  end

  def owner_profile_update
  end

  def owner_license
    @license = License.new
  end

  def owner_license_update
  end

  def owner_notification
  end

  def owner_account
    @owner = current_user.try(:owner)
  end

  def owner_account_create
    account = Account.new(account_params)
    account.save
    redirect_to owner_account_path
  end

  def owner_account_update
    account = Account.find(params[:id])
    account.update(account_params)
    redirect_to owner_account_path
  end

  def owner_privacy
  end

  def owner_password
  end

  def owner_detail
  end

  def owner_create
    owner = Owner.new(owner_params)
    store = Store.find(owner_params[:store_id])
    car_name = owner_params[:car_name]
    if owner.save
      current_user.update(owner_id: owner.id)
    end
    OwnerEntryMailer.entry_notification(owner, store, car_name).deliver_now
    redirect_to ownerpages_path
  end

  def store_select
    @store = Store.find(params[:store_id])
  end

  private

  def owner_params
    params.require(:owner).permit!
  end

  def license_params
    params.require(:license).permit!
  end

  def account_params
    params.require(:account).permit!
  end


end
