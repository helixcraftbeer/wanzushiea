class ReservesController < ApplicationController

  def reserve01
  end

  def reserve02
    @provisional_reservation = ProvisionalReservation.find_by(user_id: current_user.id, id: params[:res_id])
    @car = @provisional_reservation.car
  end

  def reserve03
    @provisional_reservation = ProvisionalReservation.find_by(user_id: current_user.id, id: params[:res_id])
    @car = @provisional_reservation.car
  end

  def reserve_done
    provisional_reservation = ProvisionalReservation.find_by(user_id: current_user.id, id: params[:res_id])
    #　お支払い
    reservation = Reservation.new
    reservation.car_id = provisional_reservation.car_id
    reservation.user_id = provisional_reservation.user_id
    reservation.state = "acceptance"
    reservation.number = provisional_reservation.number
    reservation.insurance_fee = provisional_reservation.insurance_fee
    reservation.price = provisional_reservation.price
    reservation.total = provisional_reservation.total
    reservation.start_date = provisional_reservation.start_date
    reservation.end_date = provisional_reservation.end_date
    reservation.start_time = provisional_reservation.start_time
    reservation.end_time = provisional_reservation.end_time
    reservation.payment = true
    reservation.save
    redirect_to mypages_path
  end

  def pay
    car = Car.find(params[:car_id])
    provisional_reservation = ProvisionalReservation.find(params[:p_r_id])
    user = current_user
    store = Store.find(car.store_id)
    if car.owner
      owner = car.owner
      PaymentMailer.owner_notification(owner, car, provisional_reservation).deliver_now
    end
    PaymentMailer.user_notification(car, user, provisional_reservation, store).deliver_now
    if store.email
      PaymentMailer.store_notification(store, car, provisional_reservation).deliver_now
    end

    Payjp.api_key = 'sk_test_3c4fcc6d32f9a15baaac339f'
    Payjp::Charge.create(
      amount: provisional_reservation.total,
      card: params['payjp-token'],
      currency: 'jpy'
    )
    #　お支払い
    reservation = Reservation.new
    reservation.car_id = provisional_reservation.car_id
    reservation.user_id = provisional_reservation.user_id
    reservation.state = "acceptance"
    reservation.number = provisional_reservation.number
    reservation.insurance_fee = provisional_reservation.insurance_fee
    reservation.price = provisional_reservation.price
    reservation.total = provisional_reservation.total
    reservation.start_date = provisional_reservation.start_date
    reservation.end_date = provisional_reservation.end_date
    reservation.start_time = provisional_reservation.start_time
    reservation.end_time = provisional_reservation.end_time
    reservation.payment = true
    reservation.save
    redirect_to mypages_path
  end
end
