class AdminsController < ApplicationController
  require 'geocoder'

  def index
    @stores = Store.page(params[:page]).per(5)
  end

  def admin_store
  end

  def admin_store_create
    store = Store.new(store_params)
    store.save
    redirect_to admins_path
  end

  def admin_store_edit
    @store = Store.find(params[:id])
  end

  def admin_store_update
    store = Store.find(params[:id])
    store.update(store_params)
    redirect_to admins_path
  end

  def admin_license
    @licenses = License.page(params[:page]).per(10)
  end

  def admin_license_approval
    license = License.find(params[:id])
    license.update(approval: true)
    redirect_to admin_license_path
  end

  def admin_user
    @users = User.page(params[:page]).per(10)
  end

  def role_change
    user = User.find(params[:id])
    user.roles = []
    user.add_role params[:role] if (params[:role] == "admin" || params[:role] == "contracted")
  end

  def store_change
    user = User.find(params[:id])
    store = Store.find_by(name: params[:store])
    user.update(store_id: store.id)
  end

  def admin_reservation
    @reservations = Reservation.page(params[:page]).per(10)
  end

  def admin_return_car
    reservation = Reservation.find(params[:res_id])
    if params[:tab] == "rental"
      reservation.update(state: "rental")
    elsif params[:tab] == "returned"
      reservation.update(state: "returned")
      CarReturnMailer.returned_notification(reservation.user, reservation.car).deliver_now
    end
    redirect_to admin_reservation_path
  end

  private
  def store_params
    params.require(:store).permit!
  end
end
