class HomeController < ApplicationController

  def index
    @reviews = Review.order(created_at: :desc).limit(5)
    @informations = Information.order(created_at: :desc).limit(5)
  end

  def home_serach
    @tag = params[:tag]
  end
end
