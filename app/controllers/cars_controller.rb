class CarsController < ApplicationController

  def index
    @car = Car.find(params[:car_id])
    store = @car.store
    @prefecture = store.prefecture
    @prefectures = Prefecture.all
    @cars = Car.order("RANDOM()").limit(4)

    @hash = [{:lat=>store.latitude, :lng=>store.longitude}]
  end

  def reserve
    if user_signed_in?
      provisional_reservation = ProvisionalReservation.new(provisional_reservation_params)
      car = Car.find(provisional_reservation_params[:car_id])
      insurance_fee = ProvisionalReservation.insurance_fee_calculation(provisional_reservation.number)
      business_hour_start = car.store.business_hour_start
      business_hour_end = car.store.business_hour_end
      price = ProvisionalReservation.price_calculation(provisional_reservation.start_date, provisional_reservation.start_time, provisional_reservation.end_date, provisional_reservation.end_time, car.category_id, business_hour_start, business_hour_end)
      option_price = ProvisionalReservation.option_price_calculation(provisional_reservation.option_ids, provisional_reservation.start_date, provisional_reservation.start_time, provisional_reservation.end_date, provisional_reservation.end_time, business_hour_start, business_hour_end)
      total = insurance_fee + price + option_price
      provisional_reservation.insurance_fee = insurance_fee
      provisional_reservation.price = price
      provisional_reservation.option_price = option_price
      provisional_reservation.total = total
      provisional_reservation.save
      res_id = Reservation.check_reserved(car.id, provisional_reservation.id)
      if res_id.nil?
        redirect_to reserve01_path(res_id: provisional_reservation.id)
      else
        redirect_to reserved_path(car_id: car.id, res_id: res_id)
      end
    else
      redirect_to new_user_registration_path
    end
  end

  def reserved
    @reservation = Reservation.find(params[:res_id])
  end

  private
  def provisional_reservation_params
    params.require(:provisional_reservation).permit(:car_id, :user_id, :number, :start_date, :start_time, :end_date, :end_time, option_ids: [])
  end
end
