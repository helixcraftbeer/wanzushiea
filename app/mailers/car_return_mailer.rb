class CarReturnMailer < ApplicationMailer
  default from: '<ones-share@ones-share.com>'
  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.custom_mailer.test.subject
  #
  def returned_notification(user, car)
    @user = user
    @car = car

    mail(
      subject: "[ワンズシェア]レビューをお願いいたします！",
      to: @user.email
    ) do |format|
      format.text
    end
  end
end
