class PaymentMailer < ApplicationMailer
  default from: '<ones-share@ones-share.com>'
  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.custom_mailer.test.subject
  #
  def user_notification(car, user, provisional_reservation, store)
    @car = car
    @user = user
    @provisional_reservation = provisional_reservation
    @store = store

    mail(
      subject: "[ワンズシェア]予約が完了しました",
      to: @user.email
    ) do |format|
      format.text
    end
  end

  def owner_notification(owner, car, provisional_reservation)
    @owner = owner
    @user = owner.user
    @car = car
    @provisional_reservation = provisional_reservation

    mail(
      subject: "[ワンズシェア]予約が入りました",
      to: @user.email
    ) do |format|
      format.text
    end
  end

  def store_notification(store, car, provisional_reservation)
    @car = car
    @store = store
    @provisional_reservation = provisional_reservation

    mail(
      subject: "[ワンズシェア]車が予約されました",
      to: @store.email
    ) do |format|
      format.text
    end
  end
end
