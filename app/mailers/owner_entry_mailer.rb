class OwnerEntryMailer < ApplicationMailer
  default from: '<ones-share@ones-share.com>'
  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.custom_mailer.test.subject
  #
  def entry_notification(owner, store, car_name)
    @owner = owner
    @store = store
    @car_name = car_name

    mail(
      subject: "[ワンズシェア]オーナー申請通知",
      to: @store.email
    ) do |format|
      format.text
    end
  end

  def owner_notification(owner, store)
    @owner = owner
    @user = owner.user
    @store = store

    mail(
      subject: "[ワンズシェア]オーナー登録申請許可",
      to: @user.email
    ) do |format|
      format.text
    end
  end
end
